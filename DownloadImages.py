## Download images from the web  ##

import urllib.request
import random
import os
import glob

fileNames = []          # Vector to save the file names and folder names
path = os.getcwd()      # Full path to the project

# Get all the .txt files on the projet folder
def getTXTFiles ():
    os.chdir(path + "/TXT")                                                 # Full path to the TXT folder
    for file in glob.glob("*.txt"):
        fileNames.append([file, file.replace('.txt', '')])                  # Gets all the .txt file names and replaces .txt with nothing and push both variables onto an array

# Open each class file and start downloding each URL
def openIMGFile (file_name, img_folder):
    count = 1

    text_file = open(path + "/TXT/" + file_name, "r")                       # Open the .txt with the URLs
    text_line = text_file.read().replace('\n', ',')                         # Replace line breaks with (comma)s
    imgs = text_line.split(',')                                             # Create list with the URLs

    text_file.close()

    for img in imgs:
        print("Downloading ", count, "/", len(imgs))
        downloadImgs(img, img_folder)                                       # DownloadImgs method
        count += 1

# Download images function
def downloadImgs (img_url, folder_name):
    file_name = str(random.randrange(1,1000000)) + '.jpg'                   # Random name for the picture
    full_name = os.path.join(path + "/Imgs/" + folder_name, file_name)      # Full name for the picture (full path + file_name)

    # Start downloading the images from the URLs on the TXT files and throws exceptions if it cannot be donwloaded.
    try:
        urllib.request.urlretrieve(img_url, full_name)
    except urllib.request.URLError as excp:
        print(excp)

# Main Function
def main():
    getTXTFiles()

    # Calls the download function for each TXT file
    for x in fileNames:
        openIMGFile(x[0], x[1])

if __name__ == "__main__":
    main()