# Women_Hackathon_2019
[International Women Hackathon 2019](https://www.hackerearth.com/challenges/hackathon/international-womens-hackathon-2019/).

## Project Proposal
We propose the creation of a computer vision application that uses Machine Learning and image processing algorithms to recognize each element on the medical tool table and keep tracking of those during a medicual procedure, in order to prevent retained surgical bodies (RSB) cases.
The instrument tracking must be performed on four different stages: before the surgery begins, when new tools are introduced, before the surgeon closes the wound, and before the body is closed. Our application will track the tools on those same terms and display any changes on a period of 30 minutes through an user friendly interface. Additionally, the instrument nurse will also have a way to create a new counting whenever they see necessary.

The first step to create the application is the creation of the database. For this, we will use images extracted from Google Images and captured images using a webcam and a basic surgical kit. We are going to classify the tools into 7 different classes: 
* Dressing Forceps 
* Scalpels 
* Kelly Forceps 
* Needle Holder 
* Butterfly Grooved Director 
* Operating Scissors 
* Others

Each class will have enough labels in order to guarantee an accuracy over 80% for the classification method.

Once the database has been created, we divide it into training set and validation set. For both sets, we will need to build a feature map algorithm in order to extract the key characteristics for each class described above.

The next step is to use Convolutional Neural Networks (CNN) algorithms, hyperparameters modified accordingly to our needs, to execute tool classification into the 7 different classes.

Once the machine learning model has been trained and validated with the best performance, we are going to test it on real data using a webcam that captures video feed on real time, and an interactive surgical tool kit arranged as if it was a real surgery room.

The constraints for this projects are: time as we have around a month to develop a prototype of the application, the amount and direction of the light on the object of interest, object occlusion (part of an object occludes another), blood on tools, and the access to all medical tools used during surgery.

## Requirements
* Python 3.6.5
* OpenCV 4.1.0
* Imutils
* Numpy 
* Random
* Urllib.request
* Glob

## File/Folder Description
* **Imgs:** Folder where the images are donwloaded.
* **TXT**: Folder with the .txt files with images URLs. **To add URLs on .txt files, remember to NOT leave a line break at the end.**
* **[DownloadImages.py](https://bitbucket.org/MichelleMouse/women_hackathon_2019/src/master/DownloadImages.py):** Python script to donwload images from the Internet. **Run first if you have wish to re-train the model or do not have the database**.
* **[PreProcessing.py](https://bitbucket.org/MichelleMouse/women_hackathon_2019/src/master/PreProcessing.py):** Python script for image pre-processing. Image resizing, grayscale, data augmentation (noise, rotation), binarisation, labelling, etc.
