## Image pre-processing ##

import cv2
import numpy as np
import imutils
import os
import glob

img_names = []                                                                      # Vector to save the images names
img_aug = []                                                                        # Vector to save images for data augmentation
folder_name = []                                                                    # Vector to save the folders names
rtt = True
path = os.getcwd()                                                                  # Full path of the project
full_path = ""

# Get image full path
def pathImg(img, folder):
    name = img.replace('.jpg', '')
    p = path + "/Imgs/" + folder + "/" + name

    return p

# Get folder names
def folderNames():
    for directories in os.listdir(path + "/Imgs"):
        if not directories.startswith("."):                                         # Ignores hidden folders/files
            folder_name.append(directories)                                         # Push folder name onto the array

    for folder in folder_name:
        openImgs(folder)

# Open images and resize them and save them on gray sacale with the same name
def openImgs(folder):

    os.chdir(path + "/Imgs/" + folder + "/")                                        # Path to the different images folders
    for img in glob.glob("*.jpg"):                      
        if not img.startswith("."):                                                 # Ignores hidden files/folders
            img_names.append([img, folder])                                         # Gets all the .jpg and saves its name and parent folder onto one array
            img_aug.append([img, folder])

# Resize function
def resizeImg(image, folder):

    pre_path = pathImg(image, folder)
    full_path = pre_path + ".jpg"                                                   # Image full path

    # Handle the exceptions for 'ugly' images
    try:
        img = cv2.imread(full_path, cv2.IMREAD_GRAYSCALE)                           # Reads the image on gray scale
        new_img = cv2.resize(img, (520, 520))                                       # Resizes the image to 520x520 standard
        cv2.imwrite(full_path, new_img)                                             # Saves the new image on the full path with the same name

    except Exception as ex:
        img_names[img_names.index(image)][0] = "deleted"
        os.remove(full_path)                                                        # Removes the 'ugly' image from the folder
        print("Image couldn't be resized. Deleted from the database.")              # Message to let the user know an image have been deleted

# Image rotation
def rotateImg(img, folder):
    count = 1
    
    pre_path = pathImg(img, folder)

    for angle in np.arange(15, 345, 15):                                             # Angles from 15 to 345 on steps of 15 degrees
        path = pre_path + "_" + str(count) + ".jpg"                                  # Full path to save the rotated image
        img_p = pre_path + ".jpg"                                                    # Path to open the image

        image = cv2.imread(img_p, cv2.IMREAD_GRAYSCALE)

        rotated = imutils.rotate_bound(image, angle)                                 # Rotates the image
        new_rotated = cv2.resize(rotated, (520, 520))                                # Resizes the rotated image
        cv2.imwrite(path, new_rotated)                                               # Saves the rotated image saving the original name and adding _count.jpg

        img_temp = img.replace('.jpg', '') + "_" + str(count) + ".jpg"
        img_aug.append([img_temp, folder])

        count += 1

# Add some noise
def noiseImg(img, folder):

    pre_path = pathImg(img, folder)    

    path1 = pre_path + "Noisy1.jpg"
    path2 = pre_path + "Noisy2.jpg"
    path3 = pre_path + "Noisy3.jpg"

    temp = np.float64(np.copy(img))
    h = temp.shape[0]
    w = temp.shape[1]

    noise1 = np.random.randn(h, w) * 15
    noise2 = np.random.randn(h, w) * 35
    noise3 = np.random.randn(h, w) * 70

    noisy_img1 = np.zeros(temp.shape, np.float64)
    noisy_img2 = np.zeros(temp.shape, np.float64)
    noisy_img3 = np.zeros(temp.shape, np.float64)

    if len(temp.shape) == 2:
        noisy_img1 = temp + noise1
        noisy_img2 = temp + noise2
        noisy_img3 = temp + noise3

    else:
        noisy_img1[:, :, 0] = temp[:, :, 0] + noise1
        noisy_img1[:, :, 1] = temp[:, :, 1] + noise1
        noisy_img1[:, :, 2] = temp[:, :, 2] + noise1

        noisy_img2[:, :, 0] = temp[:, :, 0] + noise2
        noisy_img2[:, :, 1] = temp[:, :, 1] + noise2
        noisy_img2[:, :, 2] = temp[:, :, 2] + noise2

        noisy_img3[:, :, 0] = temp[:, :, 0] + noise3
        noisy_img3[:, :, 1] = temp[:, :, 1] + noise3
        noisy_img3[:, :, 2] = temp[:, :, 2] + noise3

    cv2.imwrite(path1, noisy_img1)
    cv2.imwrite(path2, noisy_img2)
    cv2.imwrite(path3, noisy_img3)

# Main Function
def main():

    # Get the folder names
    folderNames()

    if len(img_names) != 0:
        for img in img_names:
            resizeImg(img[0], img[1])
            if img[0] is not "deleted":
                rotateImg(img[0], img[1])

#     if len(img_aug) > len(img_names):
#         for img in img_aug:
#             noiseImg(img[0], img[1])

if __name__ == "__main__":
    main()